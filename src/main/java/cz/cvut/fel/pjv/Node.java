package cz.cvut.fel.pjv;

import java.util.Map;
import java.util.Objects;

public record Node(long id, Map<Node, Long> paths) {
    @Override
    public boolean equals(Object o) {
        if (o instanceof Node) {
            return id == ((Node) o).id && paths.equals(((Node) o).paths);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return 1;
    }
}
