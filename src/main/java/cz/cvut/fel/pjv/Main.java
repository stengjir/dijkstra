package cz.cvut.fel.pjv;

import java.util.Random;

public class Main {

    private static final Random RANDOM = new Random(0);

    public static void main(String[] args) {

        long start = System.currentTimeMillis();
        Graph graph = Graph.fromFile("./dataset.txt");
        long end = System.currentTimeMillis();
        System.out.printf("Loading of graph took %s seconds%n", (end - start) / 1000f);
        runOneToOne(graph);
        runOneToAll(graph);

    }


    private static void runOneToOne(final Graph graph) {
        for (int i = 0; i < 10; i++) {
            long start = System.currentTimeMillis();
            int startNode = RANDOM.nextInt(graph.getNumberOfNodes());
            int endNode = RANDOM.nextInt(graph.getNumberOfNodes());
            long price = graph.calculateDistanceFromOneToOne(startNode, endNode);
            long end = System.currentTimeMillis();
            System.out.printf("From %s to %s took %s seconds and cost is %s%n", startNode, endNode, (end - start) / 1000f, price);
        }
    }

    private static void runOneToAll(Graph graph) {
        for (int i = 0; i < 100; i++) {
            long start = System.currentTimeMillis();
            int startNode = RANDOM.nextInt(graph.getNumberOfNodes());
            long price = graph.calculateDistanceFromOneToAll(startNode);
            long end = System.currentTimeMillis();
            System.out.printf("From %s to all took %s seconds and cost is %s%n", startNode, (end - start) / 1000f, price);
        }
    }

}