package cz.cvut.fel.pjv;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;
import java.util.Set;
import lombok.NonNull;

class Graph {

    record DijkstraEntry(Node node, long price) implements Comparable<DijkstraEntry> {

        @Override
        public int compareTo(DijkstraEntry dijkstraEntry) {
            return Long.compare(price, dijkstraEntry.price);
        }
    }


    private final List<Node> nodes;

    private Graph(List<Node> nodes) {
        this.nodes = nodes;
    }

    public long calculateDistanceFromOneToAll(int start) {
        Queue<DijkstraEntry> queue = new PriorityQueue<>();
        queue.add(new DijkstraEntry(nodes.get(start), 0));
        Set<Node> closedNodes = new HashSet<>();
        Long sum = 0L;
        while (!queue.isEmpty()) {
            DijkstraEntry entry = queue.poll();
            if (closedNodes.contains(entry.node)) {
                continue;
            }
            closedNodes.add(entry.node);
            sum += entry.price;
            entry.node.paths()
                    .forEach((node, pathLength) -> {
                                if (!closedNodes.contains(node)) {
                                    queue.add(new DijkstraEntry(node, entry.price + pathLength));
                                }
                            }
                    );
        }
        return sum;
    }


    public long calculateDistanceFromOneToOne(int start, int end) {
        Queue<DijkstraEntry> queue = new PriorityQueue<>();
        queue.add(new DijkstraEntry(nodes.get(start), 0));
        Set<Node> closedNodes = new HashSet<>();
        Node endNode = this.nodes.get(end);
        while (!queue.isEmpty()) {
            DijkstraEntry entry = queue.poll();
            if (entry.node.equals(endNode)) {
                return entry.price;
            }
            if (closedNodes.contains(entry.node)) {
                continue;
            }
            closedNodes.add(entry.node);
            entry.node.paths().forEach((node, pathLength) -> {
                        if (!closedNodes.contains(node)) {
                            queue.add(new DijkstraEntry(node, entry.price + pathLength));
                        }
                    }
            );
        }
        throw new IllegalArgumentException(String.format("Path from %s to %s was not found", start, end));
    }

    public int getNumberOfNodes() {
        return this.nodes.size();
    }

    public static Graph fromFile(@NonNull final String filename) {
        try (FileInputStream fis = new FileInputStream(filename)) {
            Scanner scanner = new Scanner(fis);
            int numberOfNodes = scanner.nextInt();
            int numberOfEdges = scanner.nextInt();
            Graph graph = new Graph(new ArrayList<>());
            for (int i = 0; i < numberOfNodes; i++) {
                graph.nodes.add(new Node(i, new HashMap<>()));
            }
            addEdgesToGraph(scanner, numberOfEdges, graph);
            return graph;
        } catch (IOException e) {
            throw new IllegalArgumentException("Unable to find file by filename", e);
        }
    }


    private static void addEdgesToGraph(final Scanner scanner, final int numberOfEdges, final Graph graph) {
        for (int i = 0; i < numberOfEdges; i++) {
            int start = scanner.nextInt();
            int end = scanner.nextInt();
            long price = scanner.nextLong();
            Node endNode = graph.nodes.get(end);
            graph.nodes.get(start).paths().put(endNode, price);
        }
    }
}
